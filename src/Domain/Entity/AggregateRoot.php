<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\Entity;

use StraTDeS\SharedKernel\Domain\DomainEvent\DomainEvent;
use StraTDeS\SharedKernel\Domain\DomainEvent\EventStream;
use StraTDeS\VO\Single\Id;

abstract class AggregateRoot
{
    protected Id $id;
    private ?EventStream $eventStream = null;

    final public function recordThat(DomainEvent $event): void
    {
        $this->checkEventStreamIsStarted();
        $this->eventStream->addEvent($event);
    }

    final public function pullEventStream(): EventStream
    {
        $this->checkEventStreamIsStarted();
        $eventStream = $this->eventStream;

        $this->resetEventStream();

        return $eventStream;
    }

    final public function id(): Id
    {
        return $this->id;
    }

    private function resetEventStream(): void
    {
        $this->eventStream = new EventStream();
    }

    private function checkEventStreamIsStarted(): void
    {
        if (!$this->eventStream) {
            $this->eventStream = new EventStream();
        }
    }
}
