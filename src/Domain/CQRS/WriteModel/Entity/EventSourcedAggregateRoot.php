<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity;

use StraTDeS\SharedKernel\Domain\DomainEvent\EventStream;
use StraTDeS\SharedKernel\Domain\Entity\AggregateRoot;

abstract class EventSourcedAggregateRoot extends AggregateRoot
{
    final private function __construct()
    {
    }

    final public function reconstitute(
        EventStream $eventStream,
        Snapshot $snapshot = null
    ): AggregateRoot
    {
        if($snapshot) {
            $this->applySnapshot($snapshot);
        }

        $this->applyEventStream($eventStream);

        return $this;
    }

    final public static function createEmpty(): static
    {
        return new static();
    }

    private function applyEventStream(EventStream $eventStream): void
    {
        foreach ($eventStream->getEvents() as $event) {
            $eventName = $this->className(get_class($event));
            $applierName = "apply$eventName";
            $this->$applierName($event);
        }
    }

    private function className($className): string
    {
        if ($pos = strrpos($className, '\\')) return substr($className, $pos + 1);
        else {
            throw new \InvalidArgumentException("$className is not a valid class name");
        }
    }

    public abstract function generateSnapshot(): Snapshot;
    protected abstract function applySnapshot(Snapshot $snapshot);
}
