<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Repository;

use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\EventSourcedAggregateRoot;

interface WriteRepository
{
    public function save(EventSourcedAggregateRoot $entity): void;
}