<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\DomainEvent;

use StraTDeS\VO\Single\Id;

abstract class DomainEvent
{
    protected $id;
    protected $userId;
    protected $aggregateId;
    protected $code;
    protected $version;
    protected $data;
    protected $createdAt;

    protected function __construct(
        Id $id,
        ?Id $userId,
        Id $aggregateId,
        string $code,
        int $version,
        array $data,
        \DateTime $createdAt
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->aggregateId = $aggregateId;
        $this->code = $code;
        $this->version = $version;
        $this->data = $data;
        $this->createdAt = $createdAt;
    }

    public static function createFromValues(
        Id $id,
        ?Id $userId,
        Id $aggregateId,
        string $code,
        int $version,
        array $data,
        \DateTime $createdAt
    ): DomainEvent
    {
        return new static(
            $id,
            $userId,
            $aggregateId,
            $code,
            $version,
            $data,
            $createdAt
        );
    }

    /**
     * @param Id $id
     * @param Id $aggregateId
     * @param array $data
     * @param \DateTime|null $createdAt
     * @param Id|null $userId
     * @return DomainEvent|static
     * @throws \Exception
     */
    final public static function fire(
        Id $id,
        Id $aggregateId,
        $data = [],
        \DateTime $createdAt = null,
        ?Id $userId = null
    ): DomainEvent {
        return new static(
            $id,
            $userId,
            $aggregateId,
            static::getDefaultCode(),
            static::getDefaultVersion(),
            $data,
            $createdAt ?? new \DateTime()
        );
    }

    final public function getId(): Id
    {
        return $this->id;
    }

    final public function getUserId(): Id
    {
        return $this->userId;
    }

    final public function getAggregateId(): Id
    {
        return $this->aggregateId;
    }

    final public function getCode(): string
    {
        return $this->code;
    }

    final public function getVersion(): int
    {
        return $this->version;
    }

    final public function getData(): array
    {
        return $this->data;
    }

    final public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    protected abstract static function getDefaultCode(): string;

    protected abstract static function getDefaultVersion(): int;
}
