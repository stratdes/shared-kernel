<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\Service;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\Query;
use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\QueryResult;

interface QueryBus
{
    public function ask(Query $query): ?QueryResult;
}