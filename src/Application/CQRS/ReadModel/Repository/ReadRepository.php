<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\Repository;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ProjectionModel;

interface ReadRepository
{
    public function get(string $id): ProjectionModel;

    public function find(string $id): ?ProjectionModel;
}