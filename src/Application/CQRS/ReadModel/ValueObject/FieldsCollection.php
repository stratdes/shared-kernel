<?php

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject;

class FieldsCollection
{
    private $fields;

    public function addQueryField(QueryField $field): FieldsCollection
    {
        $this->fields[$field->getName()] = $field;
        return $this;
    }

    public function addFromArray(array $field): FieldsCollection
    {
        $fieldName = key($field);
        $this->fields[$fieldName] = new QueryField($fieldName, $field[$fieldName]);
        return $this;
    }

    /**
     * @return QueryField[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}