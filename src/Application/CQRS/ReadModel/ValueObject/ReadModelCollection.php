<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject;

class ReadModelCollection
{
    /** @var ProjectionModel[] */
    private $items;
    private $count;
    private $total;

    public function __construct(array $items, int $total)
    {
        $this->items = $items;
        $this->count = count($items);
        $this->total = $total;
    }

    /**
     * @return ProjectionModel[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}