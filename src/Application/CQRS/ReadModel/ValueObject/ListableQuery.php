<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\Exception\InvalidFilterFieldException;
use StraTDeS\SharedKernel\Application\CQRS\ReadModel\Exception\InvalidSortFieldException;

abstract class ListableQuery extends Query
{
    private $page;
    private $perPage;
    private $filters;
    private $sortBy;

    public function __construct(
        ?int $page = 1,
        ?int $perPage = 20,
        ?array $filters = [],
        ?array $sortBy = []
    ) {
        $this->page = $page ?? 1;
        $this->perPage = $perPage ?? 20;
        $this->setFilters($filters ?? []);
        $this->setSortBy($sortBy ?? []);
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getSortBy(): ?array
    {
        return $this->sortBy;
    }

    /**
     * @param QueryField[] $filters
     * @throws InvalidFilterFieldException
     */
    private function setFilters(array $filters): void
    {
        $this->checkFiltersAreValidFilterableFields($filters);
        $this->fixTypes($filters);
        $this->filters = $filters;
    }

    private function setSortBy(array $sortBy): void
    {
        $this->checkSortByIsAValidSortableByField($sortBy);
        $this->sortBy = $sortBy;
    }

    protected abstract function getFilterableFields(): FieldsCollection;

    protected abstract function getSortableByFields(): FieldsCollection;

    private function checkSortByIsAValidSortableByField(?array $sortBy): void
    {
        foreach ($sortBy as $field => $value) {
            if (!isset($this->getSortableByFields()->getFields()[$field])) {
                $sortableFieldsNames = array_map(function(QueryField $field) {
                    return $field->getName();
                }, $this->getSortableByFields()->getFields());
                $validFields = implode(', ', $sortableFieldsNames);
                throw new InvalidSortFieldException("$field is not a valid sortable field (valid fields: $validFields)");
            }
        }
    }

    private function checkFiltersAreValidFilterableFields(?array $filters): void
    {
        foreach ($filters as $field => $value) {
            if (!isset($this->getFilterableFields()->getFields()[$field])) {
                $filterableFieldsNames = array_map(function(QueryField $field) {
                    return $field->getName();
                }, $this->getFilterableFields()->getFields());
                $validFields = implode(', ', $filterableFieldsNames);
                throw new InvalidFilterFieldException("$field is not a valid filterable field (valid fields: $validFields)");
            }
        }
    }

    private function fixTypes(array &$filters): void
    {
        foreach ($filters as $field => &$value) {
            $queryField = $this->getFilterableFields()->getFields()[$field];
            $value = $queryField->filterValue($value);
        }
    }
}