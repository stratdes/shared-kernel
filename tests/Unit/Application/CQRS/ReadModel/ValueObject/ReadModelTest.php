<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Application\CQRS\ReadModel\ValueObject;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ProjectionModel;
use PHPUnit\Framework\TestCase;

class ReadModelTest extends TestCase
{
    /**
     * @test
     */
    public function checkChangeDataActuallyChangesData()
    {
        // Arrange
        $readModel = ProjectionModel::withData(
            [
                'foo' => 'bar'
            ],
            'id',
            'foo'
        );

        // Act
        $readModel->changeData(
            [
                'bar' => 'foo'
            ]
        );

        // Assert
        $this->assertEquals([
            'bar' => 'foo'
        ], $readModel->getData());
    }

    /**
     * @test
     */
    public function checkSerializeGeneratesAProperJSONString()
    {
        // Arrange
        $readModel = ProjectionModel::withData(
            [
                'foo' => 'bar'
            ],
            'id',
            'foo'
        );

        // Act
        $json = json_encode($readModel);

        // Assert
        $this->assertEquals(json_encode([
            'foo' => 'bar'
        ]), $json);
    }
}
