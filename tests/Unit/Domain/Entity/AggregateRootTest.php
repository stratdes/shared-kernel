<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\Entity;

use StraTDeS\SharedKernel\Domain\DomainEvent\EventStream;
use PHPUnit\Framework\TestCase;
use StraTDeS\VO\Single\UUIDV1;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\DomainEventStub;

class AggregateRootTest extends TestCase
{
    /**
     * @test
     */
    public function checkRecordThatSavesEventToTheEventStream()
    {
        // Arrange
        $aggregateRoot = new AggregateRootStub(UUIDV1::generate());
        $event = DomainEventStub::fire(
            UUIDV1::generate(),
            UUIDV1::generate(),
            []
        );
        $expectedEventStream = new EventStream();
        $expectedEventStream->addEvent($event);

        // Act
        $aggregateRoot->recordThat($event);

        // Assert
        $this->assertEquals($expectedEventStream, $aggregateRoot->pullEventStream());
    }

    /**
     * @test
     */
    public function checkAggregateRootReturnsProperIdWhenConstructed()
    {
        // Arrange
        $id = UUIDV1::generate();
        $aggregateRoot = new AggregateRootStub($id);

        // Act

        // Assert
        $this->assertEquals($id, $aggregateRoot->id());
    }
}
