<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\Entity;

use StraTDeS\SharedKernel\Domain\Entity\AggregateRoot;
use StraTDeS\VO\Single\Id;

class AggregateRootStub extends AggregateRoot
{
    public function __construct(Id $id)
    {
        $this->id = $id;
    }
}
