<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent;

use StraTDeS\SharedKernel\Domain\DomainEvent\EventStream;
use PHPUnit\Framework\TestCase;
use StraTDeS\VO\Single\UUIDV1;

class EventStreamTest extends TestCase
{
    /**
     * @test
     */
    public function checkEventStreamReturnsAProperDomainEventArrayWhenSomeEventsAdded()
    {
        // Arrange
        $eventStream = new EventStream();
        $domainEvent1 = DomainEventStub::fire(
            UUIDV1::generate(),
            UUIDV1::generate(),
            []
        );
        $domainEvent2 = DomainEventStub::fire(
            UUIDV1::generate(),
            UUIDV1::generate(),
            []
        );

        // Act
        $eventStream->addEvent($domainEvent1);
        $eventStream->addEvent($domainEvent2);

        // Assert
        $this->assertEquals(
            [
                $domainEvent1,
                $domainEvent2
            ],
            $eventStream->getEvents()
        );
    }
}
