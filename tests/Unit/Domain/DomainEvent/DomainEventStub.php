<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent;

use StraTDeS\SharedKernel\Domain\DomainEvent\DomainEvent;

class DomainEventStub extends DomainEvent
{
    const DEFAULT_CODE = 'foo';
    const DEFAULT_VERSION = 1;

    protected static function getDefaultCode(): string
    {
        return self::DEFAULT_CODE;
    }

    protected static function getDefaultVersion(): int
    {
        return self::DEFAULT_VERSION;
    }
}