<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\CQRS\WriteModel\Entity;

use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\EventSourcedAggregateRoot;
use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\Snapshot;
use StraTDeS\VO\Single\Id;
use StraTDeS\VO\Single\UUIDV1;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\DomainEventStub;

class EventSourcedAggregateRootStub extends EventSourcedAggregateRoot
{
    private string $foo;
    private string $bar;

    public static function create(Id $id, string $foo, string $bar): EventSourcedAggregateRootStub
    {
        $instance = parent::createEmpty();

        $instance->recordThat(
            $event = DomainEventStub::fire(
                UUIDV1::generate(),
                $id,
                [
                    'foo' => $foo,
                    'bar' => $bar
                ]
            )
        );

        $instance->applyDomainEventStub($event);

        return $instance;
    }

    public function foo()
    {
        return $this->foo;
    }

    public function bar()
    {
        return $this->bar;
    }

    protected function applyDomainEventStub(DomainEventStub $event): void
    {
        $this->id = $event->getAggregateId();
        $this->foo = $event->getData()['foo'];
        $this->bar = $event->getData()['bar'];
    }

    protected function applySnapshot(Snapshot $snapshot)
    {
        $this->foo = $snapshot->getData()['foo'];
        $this->bar = $snapshot->getData()['bar'];
    }

    public function generateSnapshot(): Snapshot
    {
        return new Snapshot(
            [
                'id' => $this->id->getHumanReadableId(),
                'foo' => $this->foo,
                'bar' => $this->bar
            ]
        );
    }
}
