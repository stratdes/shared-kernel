<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\CQRS\WriteModel\Entity;

use PHPUnit\Framework\TestCase;
use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\Snapshot;
use StraTDeS\VO\Single\UUIDV1;

class EventSourcedAggregateRootTest extends TestCase
{
    /**
     * @test
     */
    public function checkAnAggregateItsReconstituted()
    {
        // Arrange
        $id = UUIDV1::generate();
        $aggregateRoot = EventSourcedAggregateRootStub::create($id, 'foo', 'bar');
        $eventStream = $aggregateRoot->pullEventStream();
        $reconstitutedAggregateRoot = EventSourcedAggregateRootStub::createEmpty();

        // Act
        $reconstitutedAggregateRoot->reconstitute($eventStream);

        // Assert
        $this->assertEquals($id, $reconstitutedAggregateRoot->id());
        $this->assertEquals('foo', $reconstitutedAggregateRoot->foo());
        $this->assertEquals('bar', $reconstitutedAggregateRoot->bar());
    }

    /**
     * @test
     */
    public function checkAnAggregateItsReconstitutedWithSnapshot()
    {
        // Arrange
        $id = UUIDV1::generate();
        $aggregateRoot = EventSourcedAggregateRootStub::create($id, 'foo_new', 'bar_new');
        $snapshot = new Snapshot([
            'foo' => 'foo_old',
            'bar' => 'bar_old'
        ]);
        $eventStream = $aggregateRoot->pullEventStream();
        $reconstitutedAggregateRoot = EventSourcedAggregateRootStub::createEmpty();

        // Act
        $reconstitutedAggregateRoot->reconstitute($eventStream, $snapshot);

        // Assert
        $this->assertEquals($id, $reconstitutedAggregateRoot->id());
        $this->assertEquals('foo_new', $reconstitutedAggregateRoot->foo());
        $this->assertEquals('bar_new', $reconstitutedAggregateRoot->bar());
    }
}
