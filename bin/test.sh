PRJ_DIR="$( cd "$( dirname "$0" )/.." && pwd -P )"

docker-compose -f "$PRJ_DIR/tests/docker-compose.yml" pull test-php-cli
docker-compose -f "$PRJ_DIR/tests/docker-compose.yml" run --rm test-php-cli vendor/bin/phpunit --no-coverage tests "$@"
